package com.example.matteo.addcontact;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.app.ProgressDialog;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.os.SystemClock.sleep;

public class MainActivity extends AppCompatActivity {

    String Lastname="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int Permission_All = 1;
        String[] Permissions = {
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS};
        //richieste di permessi
        if (!hasPermissions(this, Permissions)) {
            ActivityCompat.requestPermissions(this, Permissions, Permission_All);
        } else {
            lastID();
            showID();
        }

    }



    //ritorna false se i permessi NON sono stati accettati, altrimenti true
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    //metodo che è invocato quando si accettano o rofiutano i permessi
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Button btn1 = (Button) findViewById(R.id.button);
        Button btn2 = (Button) findViewById(R.id.HASwa);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted!
                    btn1.setClickable(true);
                    btn2.setClickable(true);
                    showID();
                } else {
                    // permission denied!
                    btn1.setClickable(false);
                    btn2.setClickable(false);
                }
                return;
            }
        }
    }



    //salva il nome dell'ultimo contatto
    private void lastID() {
        Cursor cursor =  managedQuery(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        Lastname = String.valueOf(cursor.getCount());
    }

    //scrive come si chiamerà il successivo contatto (nome)
    private void showID() {
        String id = Lastname;
        TextView textView = findViewById(R.id.textView);
        if (id == "0")
            textView.setText("There aren't contacts");
        else {
            textView.setText("The next contact's name will be " + (Integer.parseInt(id)));//perchè +2 ?
        }
    }

    //prelievo numero di partenza
    private long startNumber() {
        EditText editText = (EditText) findViewById(R.id.startNumber);
        String nu = editText.getText().toString();
        long number = Long.parseLong(nu);
        return number;
    }

    //prelievo il numero di contatti successivi da inserire
    private int nContact() {
        EditText editText = (EditText) findViewById(R.id.nContact);
        String ad = editText.getText().toString();
        int addi = Integer.valueOf(ad);
        return addi;
    }


    //crea e inserisce i contatti
    public void CreateContact(View view) {
        String name = Lastname;
        int Namenum = Integer.valueOf(name)+1;
        //prelievo numero di partenza
        long number = startNumber();
        //prelievo il numero di contatti successivi da inserire
        int addi = nContact();
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();//ContentProviderOperation permette di eseguire il comando su tutti gli elementi
                                                                                            //Log.i("CREATION", String.valueOf(ContactsContract.RawContacts.CONTENT_URI)); --> content://com.android.contacts/raw_contacts
        for (int i = 0; i < addi; i++) {
            //se l'array arriva a 100 contatti li carico e lo svuoto
            if(i%100==0) {
                try {
                    ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                } catch (RemoteException e) {
                    Log.i("RemoteException", e.getMessage());
                } catch (OperationApplicationException e) {
                    Log.i("OperationAppException", e.getMessage());
                }
                ops=new ArrayList<ContentProviderOperation>();
            }
            int rawContactInsertIndex = ops.size();//salvo nella variabile la grandezza di ops
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.RawContacts.CONTENT_URI) //rawcontacts inserisce dati relativi all'account
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)//mette nella sezione del tipo di account la sringa
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .build());//mette nella sezione del nome di account la sringa
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)//Data = crea la sezione per i dati del contatto e metadati URI= e' il tipo che serve per salvare i contatti
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, Namenum) // Name of person
                    .build());
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, "+39" + number) // Number of person
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build()); // Type of mobile number

            number = number + 1;
            Namenum=Namenum+1;
        }
        try {
            if(ops.size()!=0) {
                ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            }

            Toast.makeText(MainActivity.this, "Contacts added", Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            Log.i("RemoteException", e.getMessage());
        } catch (OperationApplicationException e) {
            Log.i("OperationAppException", e.getMessage());
        }
        lastID();
        showID();
    }


    //faccio partire una nuova schermata premendo il bottone
    public void viewlistWA(View view) {
        Intent intent = new Intent(MainActivity.this, displayWAcontacts.class);
        startActivity(intent);
    }

    //ritorna l'id di un numero di telefono
    private String returnid(String phoneNumber){
        ContentResolver contentResolver = this.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] projection = new String[] { ContactsContract.PhoneLookup._ID};
        Cursor cursor = contentResolver.query(uri, projection, null, null, null);
        String contactId=null;
        if(cursor!=null) {
            while(cursor.moveToNext()){
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }
        return contactId;
    }

    public void open_conversation(View view) {
        Intent intent = new Intent(MainActivity.this, openWa.class);
        startActivity(intent);

    }
}
