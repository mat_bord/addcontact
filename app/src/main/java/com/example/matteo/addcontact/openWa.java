package com.example.matteo.addcontact;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class openWa extends AppCompatActivity {

    private int count=0;
    private static DatabaseHelper mDatabaseHelper;
    private static Cursor data ;
    private static final String FILE_NAME="phonenumber.txt";
    OutputStreamWriter fos=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_wa);
        mDatabaseHelper = new DatabaseHelper(this);
        data= mDatabaseHelper.getItemNumber();
        File dir = getFilesDir();
        File file = new File(dir, FILE_NAME);
        boolean deleted = file.delete();
    }


    @Override
    protected void onResume() {
        try {
            fos= new OutputStreamWriter(this.openFileOutput(FILE_NAME, MODE_APPEND));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        super.onResume();
        if (data.moveToNext()){
            Uri uri = Uri.parse("smsto:" + data.getString(0));
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setPackage("com.whatsapp");
            startActivity(i);
            try {
                fos.write(count+") "+data.getString(0)+ "\r\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            count++;
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            TextView textView = findViewById(R.id.finish);
            textView.setText("Finish, I took "+count+" profile pictures");
            Toast.makeText(this,"saved to "+getFilesDir()+"/"+FILE_NAME, Toast.LENGTH_LONG).show();

        }
    }
}
