package com.example.matteo.addcontact;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;


public class displayWAcontacts extends AppCompatActivity{

    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_wacontacts);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDatabaseHelper = new DatabaseHelper(this);
        hasWhatsapp();
    }

    //se il numero ha whatsapp lo scrive
    private void hasWhatsapp()  {
        //This class provides applications access to the content model
        TextView textView = findViewById(R.id.ncontact);
        LongRunningTask myTask=new LongRunningTask(this,textView);
        myTask.execute();


    }

    private class LongRunningTask extends AsyncTask<Void, Void, ArrayList<String>>{
        Context context;
        TextView textView;
        ProgressDialog progressDialog;
        LongRunningTask(Context context, TextView textView){
            this.context=context;
            this.textView=textView;
        }


        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            mDatabaseHelper.upgradedb();
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            ContentResolver cr = context.getContentResolver();

            //RowContacts for filter Account Types
            Cursor contactCursor = cr.query(
                    ContactsContract.RawContacts.CONTENT_URI,
                    new String[]{ContactsContract.RawContacts._ID,
                            ContactsContract.RawContacts.CONTACT_ID},
                    ContactsContract.RawContacts.ACCOUNT_TYPE + "= ?",
                    new String[]{"com.whatsapp"},
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");//ordino per nome i contatti
            Log.w(" dimension :" , String.valueOf(contactCursor.getCount()));
            //array per salvare la frase da stampare con numeri e nomi
            ArrayList<String> myWhatsappContacts = new ArrayList<>();
            if (contactCursor != null) {
                if (contactCursor.getCount() > 0) {
                    if (contactCursor.moveToFirst()) {
                        do {
                            //whatsappContactId for get Number,Name,Id ect... from  ContactsContract.CommonDataKinds.Phone
                            String whatsappContactId = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));

                            if (whatsappContactId != null) {
                                //Get Data from ContactsContract.CommonDataKinds.Phone of Specific CONTACT_ID
                                Cursor whatsAppContactCursor = cr.query(
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                        new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                        new String[]{whatsappContactId}, null);

                                if (whatsAppContactCursor != null) {
                                    whatsAppContactCursor.moveToFirst();
                                    String id = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                                    String name = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                    String number = whatsAppContactCursor.getString(whatsAppContactCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                                    boolean insertData = mDatabaseHelper.addData(id, number);
                                    whatsAppContactCursor.close();

                                    //Add Number and name to ArrayList
                                    myWhatsappContacts.add("name:"+name+" number:"+number);
                                    /*Log.w( " WhatsApp id  :" , id);
                                    Log.w(" WhatsAppname :" ,name);
                                    Log.w(" WhatsApp number :" ,number);*/
                                }
                            }
                        } while (contactCursor.moveToNext());
                        contactCursor.close();
                    }
                }
            }
            return myWhatsappContacts;
        }


        @Override
        protected void onPostExecute(ArrayList<String> myWhatsappContacts ) {
            super.onPostExecute(myWhatsappContacts);
            Log.w(" WhatsApp size:" , String.valueOf(myWhatsappContacts.size()));

            //TextView textView = findViewById(R.id.ncontact);
            textView.setText("There are "+String.valueOf(myWhatsappContacts.size())+" contacts that have whatsapp ");
            ArrayAdapter<String> adapter=new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,myWhatsappContacts);
            ListView listView =findViewById(R.id.list);
            listView.setAdapter(adapter);
            progressDialog.hide();
        }
    }


}


